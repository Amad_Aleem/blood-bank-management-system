/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a;
import static a.Login_Form.url;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Muhammad Amad
 */
public class AddEmployee extends javax.swing.JFrame
{
    /**
     * Creates new form AddEmployee
     */
    public AddEmployee()
    {
        initComponents();
    }

    Connection conn = null;

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        AdminArea = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        Name = new javax.swing.JLabel();
        Password = new javax.swing.JLabel();
        Designation = new javax.swing.JLabel();
        Address = new javax.swing.JLabel();
        Reset = new javax.swing.JButton();
        Save = new javax.swing.JButton();
        Back = new javax.swing.JButton();
        passField = new javax.swing.JTextField();
        addressField = new javax.swing.JTextField();
        nameField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        postsCombo = new javax.swing.JComboBox<>();
        CNIC = new javax.swing.JLabel();
        cnicField = new javax.swing.JTextField();
        PicLable = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Add Employees");
        setMinimumSize(new java.awt.Dimension(1920, 1080));
        getContentPane().setLayout(null);

        AdminArea.setFont(new java.awt.Font("Times New Roman", 1, 70)); // NOI18N
        AdminArea.setForeground(new java.awt.Color(0, 0, 0));
        AdminArea.setText("Admin's Area");
        getContentPane().add(AdminArea);
        AdminArea.setBounds(160, 90, 650, 100);

        jPanel1.setBorder(javax.swing.BorderFactory.createMatteBorder(4, 4, 4, 4, new java.awt.Color(0, 0, 0)));

        Name.setFont(new java.awt.Font("Times New Roman", 1, 22)); // NOI18N
        Name.setText("Name");

        Password.setFont(new java.awt.Font("Times New Roman", 1, 22)); // NOI18N
        Password.setText("New Password");

        Designation.setFont(new java.awt.Font("Times New Roman", 1, 22)); // NOI18N
        Designation.setText("Designation");

        Address.setFont(new java.awt.Font("Times New Roman", 1, 22)); // NOI18N
        Address.setText("Address");

        Reset.setFont(new java.awt.Font("Times New Roman", 1, 22)); // NOI18N
        Reset.setText("Reset");
        Reset.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                ResetActionPerformed(evt);
            }
        });

        Save.setFont(new java.awt.Font("Times New Roman", 1, 22)); // NOI18N
        Save.setText("Save");
        Save.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                SaveActionPerformed(evt);
            }
        });

        Back.setFont(new java.awt.Font("Times New Roman", 1, 22)); // NOI18N
        Back.setText("Back");
        Back.setMaximumSize(new java.awt.Dimension(245, 44));
        Back.setMinimumSize(new java.awt.Dimension(245, 44));
        Back.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                BackActionPerformed(evt);
            }
        });

        passField.setFont(new java.awt.Font("Times New Roman", 0, 22)); // NOI18N

        addressField.setFont(new java.awt.Font("Times New Roman", 0, 22)); // NOI18N

        nameField.setFont(new java.awt.Font("Times New Roman", 0, 22)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 30)); // NOI18N
        jLabel1.setText("Adding New Employee");
        jLabel1.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));

        postsCombo.setFont(new java.awt.Font("Times New Roman", 0, 22)); // NOI18N
        postsCombo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Employee", "Admin" }));

        CNIC.setFont(new java.awt.Font("Times New Roman", 1, 22)); // NOI18N
        CNIC.setText("CNIC");

        cnicField.setFont(new java.awt.Font("Times New Roman", 0, 22)); // NOI18N
        cnicField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cnicFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(215, 215, 215)
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Back, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Address)
                            .addComponent(Name)
                            .addComponent(Password)
                            .addComponent(Designation)
                            .addComponent(CNIC))
                        .addGap(3, 3, 3)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(125, 125, 125)
                        .addComponent(Reset)
                        .addGap(125, 125, 125)
                        .addComponent(Save)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 94, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cnicField)
                            .addComponent(passField, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                            .addComponent(addressField, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                            .addComponent(nameField, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                            .addComponent(postsCombo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(78, 78, 78)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Name)
                    .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Password)
                    .addComponent(passField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CNIC, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cnicField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Designation)
                    .addComponent(postsCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Address)
                    .addComponent(addressField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 94, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Reset)
                    .addComponent(Back, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Save))
                .addGap(78, 78, 78))
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(160, 220, 750, 610);

        PicLable.setIcon(new javax.swing.ImageIcon("D:\\Uni_Data\\Blood Donation System Project (Modified)\\Project\\Images\\admin 1.jpg")); // NOI18N
        getContentPane().add(PicLable);
        PicLable.setBounds(0, 0, 2050, 1080);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ResetActionPerformed
        nameField.setText("");
        passField.setText("");
        cnicField.setText("");
        addressField.setText("");
        postsCombo.setSelectedIndex(0);
        nameField.grabFocus();
    }//GEN-LAST:event_ResetActionPerformed

    private void SaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveActionPerformed
        try
        {
            conn = DriverManager.getConnection(url);
            String date = "select getdate()";
            PreparedStatement st1 = conn.prepareStatement(date);
            ResultSet rs = st1.executeQuery();
            if (rs.next())
            {
                Date joindate = rs.getDate(1);
                String query = "insert into Employee (E_name, E_pass, E_CNIC,E_designation, E_address, Added_on) values (?,?,?,?,?,?)";
                PreparedStatement st = conn.prepareStatement(query);
                st.setString(1, nameField.getText());
                st.setString(2, passField.getText());
                st.setString(3, cnicField.getText());
                String pst;
                pst = postsCombo.getSelectedItem().toString();
                st.setString(4, pst);
                st.setString(5, addressField.getText());
                st.setDate(6, joindate);
                if ("".equals(nameField.getText()) || "".equals(passField.getText()) || "".equals(addressField.getText()) || "".equals(cnicField.getText()))
                {
                    JOptionPane.showMessageDialog(null, "Please fill the complete form!", "Error", JOptionPane.ERROR_MESSAGE);
                }
                else
                {
                    st.executeUpdate();
                    JOptionPane.showMessageDialog(null, pst + " Added Successfully!");
                }
            }
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_SaveActionPerformed

    private void BackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BackActionPerformed
        Edit_Employee adm = new Edit_Employee();
        adm.setExtendedState(MAXIMIZED_BOTH);
        adm.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BackActionPerformed

    private void cnicFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cnicFieldActionPerformed
    {//GEN-HEADEREND:event_cnicFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cnicFieldActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(AddEmployee.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (InstantiationException ex)
        {
            java.util.logging.Logger.getLogger(AddEmployee.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (IllegalAccessException ex)
        {
            java.util.logging.Logger.getLogger(AddEmployee.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (javax.swing.UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(AddEmployee.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new AddEmployee().setVisible(true);
            }

        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Address;
    private javax.swing.JLabel AdminArea;
    private javax.swing.JButton Back;
    private javax.swing.JLabel CNIC;
    private javax.swing.JLabel Designation;
    private javax.swing.JLabel Name;
    private javax.swing.JLabel Password;
    private javax.swing.JLabel PicLable;
    private javax.swing.JButton Reset;
    private javax.swing.JButton Save;
    private javax.swing.JTextField addressField;
    private javax.swing.JTextField cnicField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField nameField;
    private javax.swing.JTextField passField;
    private javax.swing.JComboBox<String> postsCombo;
    // End of variables declaration//GEN-END:variables
}
