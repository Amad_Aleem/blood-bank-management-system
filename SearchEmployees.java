/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a;
import static a.Login_Form.url;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import net.proteanit.sql.DbUtils;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Muhammad Amad
 */
public class SearchEmployees extends javax.swing.JFrame
{
    ArrayList<Employee> empList = new ArrayList<Employee>();
    Connection conn = null;

    /**
     * Creates new form AllEmployees
     */
    public SearchEmployees()
    {
        try
        {
            initComponents();
            ShowEmp();
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public ArrayList<Employee> EmpList() throws SQLException
    {
        try
        {
            conn = DriverManager.getConnection(url);
            String query1 = "select * from Employee";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(query1);
            Employee emp;
            while (rs.next())
            {
                emp = new Employee(rs.getInt("E_id"), rs.getString("E_name"), rs.getString("E_pass"), rs.getString("E_cnic"), rs.getString("E_designation"), rs.getString("E_address"), rs.getDate("Added_on"));
                empList.add(emp);
            }
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return empList;
    }

    public void ShowEmp() throws SQLException
    {
        try
        {
            ArrayList<Employee> list = EmpList();
            DefaultTableModel model = (DefaultTableModel) AllEmpTable.getModel();
            Object[] row = new Object[7];
            for (int i = 0; i < list.size(); i++)
            {
                row[0] = list.get(i).getE_id();
                row[1] = list.get(i).getE_name();
                row[2] = list.get(i).getE_pass();
                row[3] = list.get(i).getE_cnic();
                row[4] = list.get(i).getE_designation();
                row[5] = list.get(i).getE_address();
                row[6] = list.get(i).getAdded_on();
                model.addRow(row);
            }
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        AllEmpTable = new javax.swing.JTable();
        Back = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        postsCombo = new javax.swing.JComboBox<>();
        SearchField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        AdminArea = new javax.swing.JLabel();
        PicLable = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createMatteBorder(4, 4, 4, 4, new java.awt.Color(0, 0, 0)));

        AllEmpTable.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(1, 1, 1)));
        AllEmpTable.setFont(new java.awt.Font("Times New Roman", 0, 20)); // NOI18N
        AllEmpTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "ID", "Name", "Password", "CNIC", "Designation", "Address", "Added on"
            }
        )
        {
            boolean[] canEdit = new boolean []
            {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        AllEmpTable.setName(""); // NOI18N
        AllEmpTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(AllEmpTable);

        Back.setFont(new java.awt.Font("Times New Roman", 1, 22)); // NOI18N
        Back.setText("Back");
        Back.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                BackActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 30)); // NOI18N
        jLabel1.setText("Searching Employees");
        jLabel1.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));

        postsCombo.setFont(new java.awt.Font("Calibri", 0, 20)); // NOI18N
        postsCombo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ID", "Name", "CNIC", "Designation", "Address" }));
        postsCombo.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                postsComboActionPerformed(evt);
            }
        });

        SearchField.setFont(new java.awt.Font("Calibri", 0, 20)); // NOI18N
        SearchField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                SearchFieldActionPerformed(evt);
            }
        });
        SearchField.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyReleased(java.awt.event.KeyEvent evt)
            {
                SearchFieldKeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 22)); // NOI18N
        jLabel2.setText("Please select the property ");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 22)); // NOI18N
        jLabel3.setText("below to search against:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(720, 720, 720)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(postsCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(SearchField, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(154, 154, 154)
                                .addComponent(Back, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1358, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(120, 120, 120)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addGap(62, 62, 62)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(postsCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SearchField, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 313, Short.MAX_VALUE)
                .addComponent(Back, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(117, 117, 117))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 634, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(30, 170, 1860, 760);

        AdminArea.setFont(new java.awt.Font("Times New Roman", 1, 70)); // NOI18N
        AdminArea.setForeground(new java.awt.Color(0, 0, 0));
        AdminArea.setText("Admin's Area");
        getContentPane().add(AdminArea);
        AdminArea.setBounds(30, 80, 650, 100);

        PicLable.setIcon(new javax.swing.ImageIcon("D:\\Uni_Data\\Blood Donation System Project (Modified)\\Project\\Images\\admin 1.jpg")); // NOI18N
        getContentPane().add(PicLable);
        PicLable.setBounds(0, 0, 2050, 1080);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SearchFieldKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_SearchFieldKeyReleased
    {//GEN-HEADEREND:event_SearchFieldKeyReleased
        try
        {
            conn = DriverManager.getConnection(url);
            String pst = postsCombo.getSelectedItem().toString();
            if ("ID".equals(pst))
            {
                int id = Integer.parseInt(SearchField.getText());
                String query1 = "select * from Employee where E_id=" + id;
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(query1);
                AllEmpTable.setModel(DbUtils.resultSetToTableModel(rs));
            }
            else if ("Name".equals(pst))
            {
                String name = SearchField.getText();
                String query1 = "select * from Employee where E_name='" + name + "'";
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(query1);
                AllEmpTable.setModel(DbUtils.resultSetToTableModel(rs));
            }
            else if ("CNIC".equals(pst))
            {
                String name = SearchField.getText();
                String query1 = "select * from Employee where E_CNIC='" + name + "'";
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(query1);
                AllEmpTable.setModel(DbUtils.resultSetToTableModel(rs));
            }
            else if ("Designation".equals(pst))
            {
                String name = SearchField.getText();
                String query1 = "select * from Employee where E_designation='" + name + "'";
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(query1);
                AllEmpTable.setModel(DbUtils.resultSetToTableModel(rs));
            }
            else if ("Address".equals(pst))
            {
                String name = SearchField.getText();
                String query1 = "select * from Employee where E_address='" + name + "'";
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(query1);
                AllEmpTable.setModel(DbUtils.resultSetToTableModel(rs));
            }
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_SearchFieldKeyReleased

    private void SearchFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_SearchFieldActionPerformed
    {//GEN-HEADEREND:event_SearchFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SearchFieldActionPerformed

    private void postsComboActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_postsComboActionPerformed
    {//GEN-HEADEREND:event_postsComboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_postsComboActionPerformed

    private void BackActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_BackActionPerformed
    {//GEN-HEADEREND:event_BackActionPerformed
        Edit_Employee abc = new Edit_Employee();
        abc.setExtendedState(MAXIMIZED_BOTH);
        abc.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BackActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(SearchEmployees.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (InstantiationException ex)
        {
            java.util.logging.Logger.getLogger(SearchEmployees.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (IllegalAccessException ex)
        {
            java.util.logging.Logger.getLogger(SearchEmployees.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (javax.swing.UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(SearchEmployees.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new SearchEmployees().setVisible(true);
            }

        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel AdminArea;
    private javax.swing.JTable AllEmpTable;
    private javax.swing.JButton Back;
    private javax.swing.JLabel PicLable;
    private javax.swing.JTextField SearchField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> postsCombo;
    // End of variables declaration//GEN-END:variables
}
