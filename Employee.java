package a;
import java.util.Date;

class Employee
{
    private int E_id;
    private String E_name, E_pass, E_designation, E_address, E_cnic;
    Date Added_on;

    public Employee(int E_id, String E_name, String E_pass, String E_cnic, String E_designation, String E_address,Date Added_on)
    {
        this.E_id = E_id;
        this.E_name = E_name;
        this.E_pass = E_pass;
        this.E_designation = E_designation;
        this.E_address = E_address;
        this.E_cnic = E_cnic;
        this.Added_on=Added_on;
    }

    public Date getAdded_on()
    {
        return Added_on;
    }

    public void setAdded_on(Date Added_on)
    {
        this.Added_on = Added_on;
    }

    public int getE_id()
    {
        return E_id;
    }

    public void setE_id(int E_id)
    {
        this.E_id = E_id;
    }

    public String getE_name()
    {
        return E_name;
    }

    public void setE_name(String E_name)
    {
        this.E_name = E_name;
    }

    public String getE_pass()
    {
        return E_pass;
    }

    public void setE_pass(String E_pass)
    {
        this.E_pass = E_pass;
    }

    public String getE_designation()
    {
        return E_designation;
    }

    public void setE_designation(String E_designation)
    {
        this.E_designation = E_designation;
    }

    public String getE_address()
    {
        return E_address;
    }

    public void setE_address(String E_address)
    {
        this.E_address = E_address;
    }

    public String getE_cnic()
    {
        return E_cnic;
    }

    public void setE_cnic(String E_cnic)
    {
        this.E_cnic = E_cnic;
    }

}
