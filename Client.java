package a;
import java.util.Date;

public class Client
{
    private int C_id;
    private String C_name, C_cnic, C_number;
    Date Added_on;

    public Client(int C_id, String C_name, String C_cnic, String C_number, Date Added_on)
    {
        this.C_id = C_id;
        this.C_name = C_name;
        this.C_cnic = C_cnic;
        this.C_number = C_number;
        this.Added_on = Added_on;
    }

    public int getC_id()
    {
        return C_id;
    }

    public void setC_id(int C_id)
    {
        this.C_id = C_id;
    }

    public String getC_name()
    {
        return C_name;
    }

    public void setC_name(String C_name)
    {
        this.C_name = C_name;
    }

    public String getC_cnic()
    {
        return C_cnic;
    }

    public void setC_cnic(String C_cnic)
    {
        this.C_cnic = C_cnic;
    }

    public String getC_number()
    {
        return C_number;
    }

    public void setC_number(String C_number)
    {
        this.C_number = C_number;
    }

    public Date getAdded_on()
    {
        return Added_on;
    }

    public void setAdded_on(Date Added_on)
    {
        this.Added_on = Added_on;
    }

}
