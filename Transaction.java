
package a;
import java.util.Date;

public class Transaction
{
    private int T_id,C_id,B_id,E_id;
    private Date T_date;
    private String T_type;

    public Transaction(int T_id,Date T_date,  String T_type, int C_id, int B_id, int E_id)
    {
        this.T_id = T_id;
        this.C_id = C_id;
        this.B_id = B_id;
        this.E_id = E_id;
        this.T_date = T_date;
        this.T_type = T_type;
    }

    public int getT_id()
    {
        return T_id;
    }

    public void setT_id(int T_id)
    {
        this.T_id = T_id;
    }

    public int getC_id()
    {
        return C_id;
    }

    public void setC_id(int C_id)
    {
        this.C_id = C_id;
    }

    public int getB_id()
    {
        return B_id;
    }

    public void setB_id(int B_id)
    {
        this.B_id = B_id;
    }

    public int getE_id()
    {
        return E_id;
    }

    public void setE_id(int E_id)
    {
        this.E_id = E_id;
    }

    public Date getT_date()
    {
        return T_date;
    }

    public void setT_date(Date T_date)
    {
        this.T_date = T_date;
    }

    public String getT_type()
    {
        return T_type;
    }

    public void setT_type(String T_type)
    {
        this.T_type = T_type;
    }

}
