package a;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class DonorManager
{
    Connect c = new Connect();
    Connection yo = c.Conn();

    void addClient(String name, String cnic, String number, Date date)
    {
        try
        {
            String query = "insert into Client (C_name,C_CNIC , C_number, Added_on) values (?,?,?,?)";
            PreparedStatement st = yo.prepareStatement(query);
            st.setString(1, name);
            st.setString(2, cnic);
            st.setString(3, number);
            st.setDate(4, date);
            st.executeUpdate();
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    ResultSet MygetDate()
    {
        try
        {
            String date_query = "select GETDATE()";
            PreparedStatement st0 = yo.prepareStatement(date_query);
            ResultSet rs1 = st0.executeQuery();
            return rs1;
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
            return null;
        }
    }

    ResultSet getmaxClientId()
    {
        try
        {
            String max_client_id = "select max(C_id) from Client";
            PreparedStatement st4 = yo.prepareStatement(max_client_id);
            ResultSet rs3 = st4.executeQuery();
            return rs3;
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
            return null;
        }
    }

}
