package a;
public class Blood
{
    private int B_id,is_available;
    private String B_group, Added_on;

    public Blood(int B_id,  String B_group,int is_available, String Added_on)
    {
        this.B_id = B_id;
        this.is_available = is_available;
        this.B_group = B_group;
        this.Added_on = Added_on;
    }

    public int getB_id()
    {
        return B_id;
    }

    public void setB_id(int B_id)
    {
        this.B_id = B_id;
    }

    public int getIs_available()
    {
        return is_available;
    }

    public void setIs_available(int is_available)
    {
        this.is_available = is_available;
    }

    public String getB_group()
    {
        return B_group;
    }

    public void setB_group(String B_group)
    {
        this.B_group = B_group;
    }

    public String getAdded_on()
    {
        return Added_on;
    }

    public void setAdded_on(String Added_on)
    {
        this.Added_on = Added_on;
    }

}
